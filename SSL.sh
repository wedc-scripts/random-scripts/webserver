#!/usr/bin/env bash
# -*- coding: utf-8 -*-

DOMAIN=$1
IP=$2
# COUNTRY=$3
# CITY=$4

## Create folder that will store the key
mkdir "$DOMAIN" && cd "$_" || echo "Folder Not Created"

## Create the rootCA key and crt
openssl req -x509 \
    -sha256 -days 356 \
    -nodes \
    -newkey rsa:2048 \
    -subj "/CN=$DOMAIN/C=RD/L=Santo Domingo" \
    -keyout "$DOMAIN"CA.key -out "$DOMAIN"CA.crt

## Create the Server Private Key
openssl genrsa -out "$DOMAIN".key 2048

## Create Certification Signing Request (CSR) config
cat >csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = RD
ST = Santo Domingo
L = Santo Domingo
O = DNI
OU = DNI IT
CN = "$DOMAIN"

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = "$DOMAIN"
IP.1 = "$IP"

EOF

## Generate Certification Signing Request (CSR) Using Server Private Key
openssl req -new -key "$DOMAIN".key -out "$DOMAIN".csr -config csr.conf

## Creating a External File
cat >cert.conf <<EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = "$DOMAIN"

EOF

## Generate SSL certification With self signed CA
openssl x509 -req \
    -in "$DOMAIN".csr \
    -CA "$DOMAIN"CA.crt -CAkey "$DOMAIN"CA.key \
    -CAcreateserial -out "$DOMAIN".crt \
    -days 365 \
    -sha256 -extfile cert.conf

## Copiar al servidor
rsync -Aavx "$DOMAIN".crt root@"$IP":/etc/ssl/certs/
rsync -Aavx "$DOMAIN".key root@"$IP":/etc/ssl/private/
