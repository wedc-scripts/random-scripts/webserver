#!/bin/bash

# Installing Apache2
zypper in -y --no-recommends w3m apache2{,-{event,utils,mod_fcgid}}

# Flags Setup
a2enflag SSL
a2enflag HTTP2

# Modules
a2enmod deflate
a2enmod fcgid
a2enmod filter
a2enmod headers
a2enmod http2
a2enmod proxy
a2enmod proxy_http
a2enmod proxy_http2
a2enmod proxy_fcgi
a2enmod rewrite
a2enmod socache_shmcb

sed -i.bak 's/APACHE_MPM="[^"]"/APACHE_MPM="event"/' /etc/sysconfig/apache2

# Firewall Setup
if [[ -x /usr/bin/firewall-cmd ]]; then
  firewall-cmd --permanent --add-service=apache2 --add-service=apache2-ssl
  firewall-cmd --reload
fi

### Mods Setup ###
# mod_deflate
cat >/etc/apache2/conf.d/mod_deflate.conf <<EOF
<IfModule mod_deflate.c>
 # Compress HTML, CSS, JavaScript, Text, XML and fonts
  AddOutputFilterByType DEFLATE application/javascript
  AddOutputFilterByType DEFLATE application/rss+xml
  AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
  AddOutputFilterByType DEFLATE application/x-font
  AddOutputFilterByType DEFLATE application/x-font-opentype
  AddOutputFilterByType DEFLATE application/x-font-otf
  AddOutputFilterByType DEFLATE application/x-font-truetype
  AddOutputFilterByType DEFLATE application/x-font-ttf
  AddOutputFilterByType DEFLATE application/x-javascript
  AddOutputFilterByType DEFLATE application/xhtml+xml
  AddOutputFilterByType DEFLATE application/xml
  AddOutputFilterByType DEFLATE font/opentype
  AddOutputFilterByType DEFLATE font/otf
  AddOutputFilterByType DEFLATE font/ttf
  AddOutputFilterByType DEFLATE image/svg+xml
  AddOutputFilterByType DEFLATE image/x-icon
  AddOutputFilterByType DEFLATE text/css
  AddOutputFilterByType DEFLATE text/html
  AddOutputFilterByType DEFLATE text/javascript
  AddOutputFilterByType DEFLATE text/plain
  AddOutputFilterByType DEFLATE text/xml

 # Remove browser bugs (only needed for really old browsers)
  BrowserMatch ^Mozilla/4 gzip-only-text/html
  BrowserMatch ^Mozilla/4\.0[678] no-gzip
  BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
  Header append Vary User-Agent
</IfModule>
EOF

# mod_headers
cat >/etc/apache2/conf.d/mod_headers.conf <<EOF
<IfModule mod_headers.c>
    Header always set X-XSS-Protection "1; mode=block"
    Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
    Header always set X-Content-Type-Options "nosniff"
    Header always set X-Permitted-Cross-Domain-Policies "none"
    Header always set Referrer-Policy "no-referrer"
    Header always set Server ""
    Header always append X-Frame-Options SAMEORIGIN
    Header always set Permissions-Policy "accelerometer=(), autoplay=(self), camera=(), cross-origin-isolated=(self), display-capture=(self), encrypted-media=(self), fullscreen=(self), geolocation=(self), gyroscope=(self), magnetometer=(self), microphone=(), payment=(self), picture-in-picture=(self), publickey-credentials-get=(self), screen-wake-lock=(self), sync-xhr=(self), usb=(), web-share=(self), xr-spatial-tracking=(), clipboard-read=(self), clipboard-write=(self)"
    #Header always edit Set-Cookie (.*) "\$1; httpOnly;Secure;SameSite=Strict"
</IfModule>
EOF

# mod_ssl
cat >/etc/apache2/conf.d/ssl.conf <<EOF
<IfModule mod_ssl.c>
        SSLProtocol             all -SSLv3 -TLSv1 -TLSv1.1 -TLSv1.2
        SSLHonorCipherOrder     off
        SSLSessionTickets       off

        SSLUseStapling On
        SSLStaplingCache "shmcb:logs/ssl_stapling(32768)"

        <IfModule mod_socache_shmcb.c>
            SSLSessionCache         shmcb:/var/lib/apache2/ssl_scache(512000)
        </IfModule>
        SSLSessionCacheTimeout  300
</IfModule>
EOF

cat >/etc/apache2/conf.d/mod_fcgid.conf <<EOF
<IfModule fcgid_module>
    FcgidIdleTimeout 900
    FcgidIdleScanInterval 300
    FcgidBusyTimeout 900
    FcgidBusyScanInterval 300

    #FcgidErrorScanInterval 3
    #FcgidZombieScanInterval 3

    FcgidProcessLifeTime 600

    FcgidIPCDir /var/lib/apache2/fcgid/
    FcgidProcessTableFile /var/lib/apache2/fcgid/shm

    FcgidOutputBufferSize 65536
</IfModule>
EOF

cat >/etc/apache2/conf.d/mod_proxy_fcgi.conf <<EOF
<IfModule mod_proxy_fcgi.c>
  <FilesMatch "\.ph(p[34578]?|tml)$">
    SetHandler "proxy:fcgi://127.0.0.1:9000"
  </FilesMatch>

  <FilesMatch "\.php[34578]?s$">
    SetHandler "proxy:fcgi://127.0.0.1:9000"
  </FilesMatch>

DirectoryIndex index.ph(p[34578])
</IfModule>
EOF

if [ ! -f "/etc/apache2/default-server.conf.old" ]; then
  mv /etc/apache2/default-server.conf /etc/apache2/default-server.conf.old
fi

if [ ! -f "/etc/apache2/httpd.conf.old" ]; then
  mv /etc/apache2/httpd.conf /etc/apache2/httpd.conf.old

  cat >/etc/apache2/httpd.conf <<EOF
Include /etc/apache2/uid.conf
Include /etc/apache2/server-tuning.conf

# Apache error_log
ErrorLog /var/log/apache2/error_log

# generated from default value of APACHE_MODULES in /etc/sysconfig/apache2
<IfDefine !SYSCONFIG>
  Include /etc/apache2/loadmodule.conf
</IfDefine>

Include /etc/apache2/listen.conf

# predefined logging formats
Include /etc/apache2/mod_log_config.conf

# generated from default values of global settings in /etc/sysconfig/apache2
<IfDefine !SYSCONFIG>
  Include /etc/apache2/global.conf
</IfDefine>

# optional mod_status, mod_info
Include /etc/apache2/mod_status.conf
Include /etc/apache2/mod_info.conf

Include /etc/apache2/mod_reqtimeout.conf
Include /etc/apache2/mod_cgid-timeout.conf

Include /etc/apache2/mod_usertrack.conf

# configuration of server-generated directory listings
Include /etc/apache2/mod_autoindex-defaults.conf

# associate MIME types with filename extensions
TypesConfig /etc/apache2/mime.types
Include /etc/apache2/mod_mime-defaults.conf

# set up (customizable) error responses
Include /etc/apache2/errors.conf

Include /etc/apache2/ssl-global.conf
Include /etc/apache2/protocols.conf

# forbid access to the entire filesystem by default
<Directory />
    Options None
    AllowOverride None
    Require all denied
</Directory>

# use .htaccess files for overriding,
AccessFileName .htaccess
# and never show them
<Files ~ "^\.ht">
    Require all denied
</Files>

# List of resources to look for when the client requests a directory
DirectoryIndex index.html index.html.var

#Include /etc/apache2/default-server.conf
IncludeOptional /etc/apache2/conf.d/*.conf
IncludeOptional /etc/apache2/vhosts.d/*.conf
IncludeOptional /etc/apache2/conf.d/apache2-manual?conf
EOF
fi

if [ ! -f "/etc/apache2/mod_reqtimeout.conf.old" ]; then
  mv /etc/apache2/mod_reqtimeout.conf /etc/apache2/mod_reqtimeout.conf.old

  cat >/etc/apache2/mod_reqtimeout.conf <<EOF
<IfModule mod_reqtimeout.c>
  # allow 10s timeout for the headers and allow 1s more until 20s upon
  # receipt of 1000 bytes.
  # almost the same with the body, except that it is tricky to
  # limit the request timeout within the body at all - it may take
  # time to generate the body.
  RequestReadTimeout header=10-30,MinRate=1000 body=30,MinRate=1000
  ProxyTimeout 900
  LogLevel reqtimeout:info
</IfModule>
EOF
fi

if [ ! -f "/etc/apache2/mod_userdir.conf.old" ]; then
  mv /etc/apache2/mod_userdir.conf /etc/apache2/mod_userdir.conf.old

  cat >/etc/apache2/mod_userdir.conf <<EOF
<IfModule mod_userdir.c>
        UserDir disabled root
        <Directory /home/*/public_html>
                AllowOverride FileInfo AuthConfig Limit Indexes
                Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec

                <Limit GET POST OPTIONS PROPFIND>
                Require all granted
                </Limit>

                <LimitExcept GET POST OPTIONS PROPFIND>
                Require all denied
                </LimitExcept>
        </Directory>
</IfModule>
EOF
fi

cat >/etc/apache2/vhosts.d/proxy.template <<EOF
<VirtualHost *:80>
    ServerName domain.com
    ServerAlias www.domain.com

    HostnameLookups Off
    UseCanonicalName Off
    ServerSignature On

    ProxyRequests Off
    ProxyPreserveHost On
    ProxyVia Full

    ErrorLog /var/log/apache2/dummy-host.example.com-error_log
    CustomLog /var/log/apache2/dummy-host.example.com-access_log combined

    <Proxy *>
        Require all granted
    </Proxy>

    ProxyPass / http://127.0.0.1:port/
    ProxyPassReverse / http://127.0.0.1:port/
</VirtualHost>
EOF

cat >/etc/apache2/vhosts.d/default.template <<EOF
<VirtualHost *:80>
    ServerName domain.com
    ServerAlias www.domain.com

    HostnameLookups Off
    UseCanonicalName Off
    ServerSignature On

    DocumentRoot "/srv/www/path"

    ErrorLog /var/log/apache2/dummy-host.example.com-error_log
    CustomLog /var/log/apache2/dummy-host.example.com-access_log combined

    <Directory "/srv/www/path"
        Options -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

# Inicia Apache2
systemctl enable --now apache2.service
