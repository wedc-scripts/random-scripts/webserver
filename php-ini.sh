#!/usr/bin/env bash
# -*- coding: utf-8 -*-

PHP_V=7

if (grep -i Leap /etc/os-release &>/dev/null); then
    zypper in --force-resolution -y php"${PHP_V}"-{cli,bcmath,bz2,calendar,ctype,curl,dom,exif,fastcgi,fileinfo,fpm,ftp,gd,gettext,gmp,iconv,intl,ldap,mysql,pgsql,mbstring,opcache,openssl,pcntl,phar,snmp,sodium,soap,tokenizer,xmlreader,xmlwriter,zip,zlib}
    # sed -i 's/\/var\/log\//\/var\/log\/php\//' /etc/apparmor.d/php-fpm
    # # Temporal change to apparmor
    # if [[ -e aa-complain ]]; then
    #     aa-complain php-fpm
    # fi
fi

if (grep -i tumbleweed /etc/os-release &>/dev/null); then
    zypper in --force-resolution -y php"${PHP_V}"-{cli,APCu,bcmath,bz2,calendar,ctype,curl,dom,exif,fastcgi,fileinfo,fpm,ftp,gd,gettext,gmp,iconv,imagick,intl,ldap,mysql,pgsql,mbstring,memcached,redis,opcache,openssl,pcntl,phar,snmp,sodium,soap,tokenizer,xmlreader,xdebug,xmlwriter,zip,zlib}
    sed -i 's/\/var\/log\//\/var\/log\/php\//' /etc/apparmor.d/php-fpm
    # Temporal change to apparmor
    if [[ -e aa-complain ]]; then
        aa-complain php-fpm
    fi
fi
if [ ! -d "/var/log/php" ]; then
    mkdir -p /var/log/php
fi

# composer-setup >>>> Start
if [ ! -f "/usr/bin/composer" ]; then
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
    rm composer-setup.php
fi

# composer-setup >>>> End

if [ ! -f "/etc/php${PHP_V}/fastcgi/php.ini.bak" ]; then
    cp /etc/php"${PHP_V}"/fastcgi/php.ini /etc/php"${PHP_V}"/fastcgi/php.ini.bak
fi

if [ ! -f "/etc/php${PHP_V}/cli/php.ini.bak" ]; then
    cp /etc/php"${PHP_V}"/cli/php.ini /etc/php"${PHP_V}"/cli/php.ini.bak
fi

if [ ! -f "/etc/php${PHP_V}/fpm/php.ini" ]; then
    ln -s /etc/php"${PHP_V}"/fastcgi/php.ini /etc/php"${PHP_V}"/fpm/
fi

# PHP.ini configuration
cat >/etc/php"${PHP_V}"/fastcgi/php.ini <<EOF
[PHP]

;;;;;;;;;;;;;;;;;;;;
; Language Options ;
;;;;;;;;;;;;;;;;;;;;
engine = On
short_open_tag = Off
precision = 14
output_buffering = 4096
zlib.output_compression = Off
implicit_flush = Off
unserialize_callback_func =
serialize_precision = -1
disable_functions =
disable_classes =
zend.enable_gc = On
zend.exception_ignore_args = On
zend.exception_string_param_max_len = 0

;;;;;;;;;;;;;;;;;
; Miscellaneous ;
;;;;;;;;;;;;;;;;;
expose_php = Off

;;;;;;;;;;;;;;;;;;;
; Resource Limits ;
;;;;;;;;;;;;;;;;;;;
max_execution_time = 900
max_input_time = 300
memory_limit = 512M

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Error handling and logging ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
display_errors = Off
display_startup_errors = Off
log_errors = On
log_errors_max_len = 1024
ignore_repeated_errors = Off
ignore_repeated_source = Off
report_memleaks = On
report_zend_debug = 0
error_log = /var/log/php/php_errors.log

;;;;;;;;;;;;;;;;;
; Data Handling ;
;;;;;;;;;;;;;;;;;
variables_order = "GPCS"
request_order = "GP"
register_argc_argv = Off
auto_globals_jit = On
post_max_size = 4G
default_mimetype = "text/html"
default_charset = "UTF-8"

;;;;;;;;;;;;;;;;;;;;;;;;;
; Paths and Directories ;
;;;;;;;;;;;;;;;;;;;;;;;;;
include_path = ".:/usr/share/php${PHP_V}:/usr/share/php${PHP_V}/PEAR"
sys_temp_dir = "/tmp"
enable_dl = Off

;;;;;;;;;;;;;;;;
; File Uploads ;
;;;;;;;;;;;;;;;;
file_uploads = On
;upload_tmp_dir =
upload_max_filesize = 2G
max_file_uploads = 5

;;;;;;;;;;;;;;;;;;
; Fopen wrappers ;
;;;;;;;;;;;;;;;;;;
allow_url_fopen = On
allow_url_include = Off
default_socket_timeout = 120

;;;;;;;;;;;;;;;;;;;
; Module Settings ;
;;;;;;;;;;;;;;;;;;;
[CLI Server]
cli_server.color = On

[Date]
date.timezone = 'America/Santo_Domingo'

[Pdo]
pdo_odbc.connection_pooling=strict

[ODBC]
odbc.allow_persistent = On
odbc.check_persistent = On
odbc.max_persistent = -1
odbc.max_links = -1
odbc.defaultlrl = 4096
odbc.defaultbinmode = 1

[MySQLi]
mysqli.max_persistent = -1
;mysqli.allow_local_infile = On
mysqli.allow_persistent = Off
mysqli.max_links = -1
mysqli.default_port = 3306
mysqli.default_socket =
mysqli.reconnect = Off

[mysqlnd]
mysqlnd.collect_statistics = On
mysqlnd.collect_memory_statistics = Off

[PostgreSQL]
pgsql.allow_persistent = On
pgsql.auto_reset_persistent = Off
pgsql.max_persistent = -1
pgsql.max_links = -1
pgsql.ignore_notice = 0
pgsql.log_notice = 0

[mail function]
; For Unix only.  You may supply arguments as well (default: "sendmail -t -i").
; http://php.net/sendmail-path
;sendmail_path =
mail.add_x_header = Off

[bcmath]
bcmath.scale = 0

[Session]
session.save_handler = files
session.save_path = "/var/lib/php${PHP_V}"
session.use_strict_mode = 1
session.use_cookies = 1
session.use_only_cookies = 1
session.name = PHPSESSID
session.auto_start = 0
session.cookie_lifetime = 0
session.cookie_path = /
session.cookie_domain =
session.cookie_httponly =
; Current valid values are "Strict", "Lax" or "None". When using "None",
session.cookie_samesite = "Strict"
session.serialize_handler = php
session.gc_probability = 1
session.gc_divisor = 1000
session.gc_maxlifetime = 1440
; http://php.net/session.referer-check
session.referer_check =
session.cache_limiter = nocache
session.cache_expire = 180
session.use_trans_sid = 0
session.sid_length = 26
session.trans_sid_tags = "a=href,area=href,frame=src,form="
session.sid_bits_per_character = 5

[Assertion]
zend.assertions = -1
assert.active = On

[Tidy]
tidy.clean_output = Off

[soap]
soap.wsdl_cache_enabled=1
soap.wsdl_cache_dir="/tmp"
soap.wsdl_cache_ttl=86400
soap.wsdl_cache_limit = 5

[ldap]
; Sets the maximum number of open links or -1 for unlimited.
ldap.max_links = -1

[opcache]
opcache.enable=1
;opcache.enable_cli=1
opcache.memory_consumption=256
opcache.interned_strings_buffer=32
opcache.max_accelerated_files=100000
;opcache.max_wasted_percentage=5
;opcache.use_cwd=1
opcache.validate_timestamps=0
opcache.revalidate_freq=1
save_comments=1
opcache.force_restart_timeout=180
opcache.error_log=/var/log/php/opcache_error.log

; By default, only fatal errors (level 0) or errors (level 1) are logged.
; You can also enable warnings (level 2), info messages (level 3) or
; debug messages (level 4).
opcache.log_verbosity_level=1

[curl]
; A default value for the CURLOPT_CAINFO option. This is required to be an
; absolute path.
;curl.cainfo =
EOF

# php-fpm.conf

cat >/etc/php"${PHP_V}"/fpm/php-fpm.conf <<EOF
;;;;;;;;;;;;;;;;;;;;;
; FPM Configuration ;
;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
; Global Options ;
;;;;;;;;;;;;;;;;;;

[global]
pid = run/php-fpm/php-fpm.pid
error_log = log/php/php-fpm.log
syslog.facility = daemon
syslog.ident = php-fpm
log_level = notice
log_limit = 4096
log_buffering = yes

emergency_restart_threshold = 10
emergency_restart_interval = 7d
process_control_timeout = 5m

; The maximum number of processes FPM will fork. This has been designed to control
; the global number of processes when using dynamic PM within a lot of pools.
; Use it with caution.
; Note: A value of 0 indicates no limit
; Default Value: 0
; process.max = 128

; Specify the nice(2) priority to apply to the master process (only if set)
; The value can vary from -19 (highest priority) to 20 (lowest priority)
; Note: - It will only work if the FPM master process is launched as root
;       - The pool process will inherit the master process priority
;         unless specified otherwise
; Default Value: no set
; process.priority = -19

daemonize = yes
systemd_interval = 10

;;;;;;;;;;;;;;;;;;;;
; Pool Definitions ;
;;;;;;;;;;;;;;;;;;;;

include=/etc/php${PHP_V}/fpm/php-fpm.d/*.conf
EOF

# www.conf - configuration

cat >/etc/php"${PHP_V}"/fpm/php-fpm.d/www.conf <<EOF
[www]
user = wwwrun
group = www

listen = 127.0.0.1:9000

pm = dynamic
pm.max_children = 120
pm.start_servers = 12
pm.min_spare_servers = 6
pm.max_spare_servers = 18

access.log = /var/log/php/\$pool.access.log
; Default: "%R - %u %t \"%m %r\" %s"
access.format = "%R - %u %t \"%m %r%Q%q\" %s %f %{mili}d %{kilo}M %C%%"
slowlog = log/php/\$pool.log.slow
catch_workers_output = yes
decorate_workers_output = yes
clear_env = yes

security.limit_extensions = .php
EOF

systemctl enable --now php-fpm.service
